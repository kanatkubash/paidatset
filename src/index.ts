import 'reflect-metadata';
import connPromise from './conn';
import * as sizeof from 'object-sizeof';
import selector from './selector';
import { Check, Where, Obj, Order } from './types';
import * as _ from 'lodash';
import {
  EntityManager,
  UpdateEvent,
  UpdateQueryBuilder,
  Connection
} from 'typeorm';
import { startTimer, stopTimer } from './debug';
import { getPaidPenalties, buildPenaltyUpdateQuery } from './penalty';
const stdLimit = 1000;
const unlim = 999999; // :)

const byIin = async () => {
  let conn = await connPromise;
  let offset = 0;
  let query = selector(conn, {
    table: 'iins',
    fields: ['number'],
    order: ['number', 'ASC'],
    limit: stdLimit
  });
  let iins: any[] = await query.execute();
  while (iins.length > 0) {
    let time: any = startTimer();
    for (let { number } of iins) {
      let checkQuery = selectQueryForCheck(conn, {
        stmt: 'c."iinNumber"=:number',
        params: {
          number
        }
      });
      let checks = await checkQuery.execute();
      checkChecks(conn, checks, 'iin', { number });
    }
    time = stopTimer(time);
    console.log(`iin ${time} ms`);
    let lastIinNum = iins[iins.length - 1].number;
    offset += stdLimit;
    iins = await selector(conn, {
      table: 'iins',
      fields: ['number'],
      order: ['number', 'ASC'],
      limit: stdLimit,
      offset
    }).execute();
  }
};

const byCar = async () => {
  let conn = await connPromise;
  let offset = 0;
  let query = selector(conn, {
    table: 'cars',
    fields: ['number', 'techPassport'],
    order: [['number', 'ASC'] as Order, ['"techPassport"', 'ASC'] as Order],
    limit: stdLimit
  });
  let cars: any[] = await query.execute();
  while (cars.length > 0) {
    let time: any = startTimer();
    for (let { number, techPassport } of cars) {
      let checkQuery = selectQueryForCheck(conn, {
        stmt: 'c."carNumber"=:number and c."carTechPassport"=:techPassport',
        params: {
          number,
          techPassport
        }
      });
      let checks = await checkQuery.execute();
      checkChecks(conn, checks, 'car', { number, techPassport });
    }
    time = stopTimer(time);
    console.log(`car ${time} ms`);
    let lastIinNum = cars[cars.length - 1].number;
    offset += stdLimit;
    cars = await selector(conn, {
      table: 'cars',
      fields: ['number', 'techPassport'],
      order: [['number', 'ASC'] as Order, ['"techPassport"', 'ASC'] as Order],
      offset
    }).execute();
  }
};

const selectQueryForCheck = (conn, where: Where) =>
  selector(conn, {
    table: 'checks',
    alias: 'c',
    fields: ['penaltyIds', 'createdAt'],
    order: ['"createdAt"', 'ASC'],
    where,
    limit: unlim
  });

const checkChecks = async (
  conn: Connection,
  checks: any[],
  by: 'iin' | 'car',
  searchedParam: Obj
) => {
  if (checks.length > 0) {
    let [penalties, wtfPenalties] = getPaidPenalties(checks);
    if (penalties.length != 0) {
      await conn.transaction(async trManager => {
        for (let { paidAt, id } of penalties) {
          await buildPenaltyUpdateQuery(id, paidAt, trManager).execute();
        }
      });
    }
    if (wtfPenalties.length != 0) {
      if (by == 'iin') {
        let { number } = searchedParam;
        console.log({ number, wtfPenalties });
      } else {
        let { number, techPassport } = searchedParam;
        console.log({ number, techPassport, wtfPenalties });
      }
    }
  }
};

(async _ => {
  Promise.all([byIin(), byCar()]).then(_ => process.exit());
})();
