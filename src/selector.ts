import 'reflect-metadata';
import connPromise from './conn';
import { Order, Where, WhereableQuery } from './types';
import * as sizeof from 'object-sizeof';
import { Connection, SelectQueryBuilder, MssqlParameter } from 'typeorm';
import whereConstruct from './where';
import orderConstruct from './order';

const missing = void 0;
export default (
  conn: Connection,
  {
    where = [],
    fields = missing,
    table,
    alias = table,
    offset = 0,
    limit = 100,
    order = []
  }: {
    where?: Where | Where[];
    fields?: string[];
    table: string;
    alias?: string;
    offset?: number;
    limit?: number;
    order?: Order | Order[];
  }
): SelectQueryBuilder<{}> => {
  let query: WhereableQuery = conn
    .createQueryBuilder()
    .select(fields ? fields.map(f => `"${f}"`) : fields)
    .from(table, alias)
    .limit(limit)
    .offset(offset);
  query = whereConstruct(query, where);
  query = orderConstruct(query as SelectQueryBuilder<{}>, order);
  return query as SelectQueryBuilder<{}>;
};
