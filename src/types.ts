import { QueryBuilder, WhereExpression } from 'typeorm';

export type Order = [string, 'ASC' | 'DESC'];
export type Where = { stmt: string; params: object };
export interface WhereableQuery extends WhereExpression, QueryBuilder<{}> {}
export type Check = { createdAt: Date; penaltyIds: string[] };
export type Obj = { [key: string]: any };
