import { Order, Where, WhereableQuery } from './types';
import {
  Connection,
  QueryBuilder,
  WhereExpression,
  SelectQueryBuilder,
  UpdateQueryBuilder
} from 'typeorm';
export default (
  query: WhereableQuery,
  where: Where | Where[]
): WhereableQuery => {
  if ('length' in where) {
    let whereArr = where as Where[];
    if (whereArr.length == 0) return query;
    return whereArr.reduce(
      (accQuery, eachWhere, index) =>
        appendWhere(accQuery, eachWhere, index != 0),
      query
    );
  } else if (where) return appendWhere(query, where as Where);
};
const appendWhere = (
  query: WhereableQuery,
  { stmt, params }: Where,
  append: boolean = false
) => (append ? query.andWhere(stmt, params) : query.where(stmt, params));
