import { UpdateQueryBuilder, EntityManager } from 'typeorm';
import { Check } from './types';
import _ = require('lodash');

export const getPaidPenalties = (
  checks: Check[]
): [{ paidAt; id }[], { id }[]] => {
  let penalties = _.chain(checks)
    .flatMap('penaltyIds')
    .uniq()
    .value();
  let updPenalties = [];
  let wtfPenalties = [];
  for (let penalty of penalties) {
    if (penalty == 'af3a82d0-9848-4b3d-83c5-157e4fa0296f') debugger;
    let paidCheck: Check = null,
      lastExistCheck = null,
      pattern = [];
    for (let check of checks) {
      let found = check.penaltyIds.indexOf(penalty) >= 0;
      if (found) {
        lastExistCheck = check;
        pattern.push(1);
      } else {
        pattern.push(0);
        if (lastExistCheck) {
          if (!paidCheck) {
            paidCheck = check;
          }
          // break;
        } else continue;
      }
    }
    if (paidCheck) {
      updPenalties.push({
        paidAt: paidCheck.createdAt,
        id: penalty
      });
    }
    pattern = pattern.filter((c, i, arr) => i === 0 || arr[i - 1] !== c);
    if (_.isEqual(pattern, [1, 0, 1])) wtfPenalties.push({ id: penalty });
  }
  return [updPenalties, wtfPenalties];
};

export const buildPenaltyUpdateQuery = (
  id: string,
  paidAt: Date,
  manager: EntityManager
): UpdateQueryBuilder<any> =>
  manager
    .createQueryBuilder()
    .update('penalties')
    .set({ paidAt })
    .where('id=:id', { id });
