export const startTimer = () => process.hrtime();
export const stopTimer = (hrstart): number => {
  var hrend = process.hrtime(hrstart);
  return hrend[0] * 1000 + Math.floor(process.hrtime(hrstart)[1] / 1000000);
};
