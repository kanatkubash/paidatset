import { Order, Where } from './types';
import { Connection, QueryBuilder, SelectQueryBuilder } from 'typeorm';
export default (
  query: SelectQueryBuilder<{}>,
  order: Order | Order[]
): SelectQueryBuilder<{}> => {
  if (order[0] instanceof Array) {
    if (order.length == 0) return query;
    return (order as Order[]).reduce(
      (accQuery, eachOrder, index) =>
        appendOrder(accQuery, eachOrder, index != 0),
      query
    );
    // for (let i = 0; i < order.length; i++) {
    //   query = appendOrder(query, (order as Order[]));
    // }
  } else return appendOrder(query, order as Order);
};
const appendOrder = (
  query: SelectQueryBuilder<{}>,
  [field, dir]: Order,
  append: boolean = false
) => (append ? query.orderBy(field, dir) : query.addOrderBy(field, dir));
